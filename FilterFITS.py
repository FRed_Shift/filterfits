import sys
from astropy.io import fits

# Check the FILTER values of the whole pictures
def mapping(first_try, *args):
    picnum=[]
    for file_in in args:
  
        #Get the FILTER value of the FITS header
        hdul = fits.open(file_in, mode='update')
        try:
            fltr0=hdul[0].header['FILTER'] 
            if fltr0 == None:
                #print("Already exists but void, FILTER = " +  str(fltr0) + "\n" )
                picnum.append('N')
            else:
                #print("Already exists, FILTER = " +  str(fltr0) + "\n" )
                picnum.append(str(fltr0))
                
        except Exception as e :
            #print("no defined FILTER field = " + "\n" )
            if first_try: #should happen only on 1st mapping   
                picnum.append('X')   
            else: #should happen only at 2nd mapping        
                hdr= hdul[0].header
                hdr.insert('GAIN', ('FILTER', None)) 
                fill_name = 'N'   
                hdr['FILTER'] = fill_name         
                hdul.flush()
                picnum.append('N')
      
        hdul.close(output_verify='fix')

    return picnum



# Change the FILTER value of all the selected pictures       
def fill_with(fill_name, *args): 
    for file_in in args:
        #Set the FILTER value of the FITS header
        hdul = fits.open(file_in, mode='update')
        hdr=hdul[0].header
        hdr['FILTER'] = fill_name            
        hdul.flush()               
        hdul.close(output_verify='fix')

    return

# Basic way to show the FILTER values of all the selectied pictures
def disp_map(first_try, argum):
    if first_try: #should happen only on 1st mapping   
        print ('Very first Filter mapping (X: no FILTER field, N: empty FILTER field)')
    else:
        print ('Filter mapping (N: empty FILTER field)')

    i=0
    for terme in argum:
        i+=1
        if i%5 == 0:
            print ('   '+str(terme)+'   '+'\n', end='')
            print (' '+str(i)+' '+'\t', end='')
        else :
            if i ==1:
                print (' '+str(i)+' '+'\t', end='')
                print ('   '+str(terme)+'   '+'\t', end='')
            else:
                print ('   '+str(terme)+'   '+'\t', end='')
    print ('')
    return


def Run(*args, keep_open=True ):   
    first_one= True
    picnum=mapping (first_one, *args)
    disp_map(first_one, picnum)
    first_one= False

    while True:
        print ('\n')
        print ('What you wanna do?')
        print ('[0]: Check mapping (and create FILTER field if needed)')
        print ('[1]: Fill all with...')
        print ('[2]: Empty all ')
        print ('[3]: Get out')
        ans=input()

        if ans == '0':
            picnum=mapping (first_one, *args)
            disp_map(first_one, picnum)
        elif ans == '1':           
            print('Filter name (3 char):')
            newfilter = input()
            fill_with(newfilter, *args)
            picnum=mapping (first_one, *args)
            disp_map(first_one, picnum)
        elif ans == '2':
            newfilter=''
            fill_with(newfilter, *args)
            picnum=mapping (first_one, *args)
            disp_map(first_one, picnum)
        elif ans == '3':
            return
        else:
            break

    print ('Stop')
    if keep_open: 
        input()
        print('Press Enter to close this window')

if __name__ == "__main__":
    Run(*sys.argv[1:])
    